import os
import time
import gtk.gdk
import Image
from subprocess import Popen, PIPE

key_coords_start = (157,24)
key_coords = (476, 55)
base_color_start = (195, 213, 234)

pieces = {
	"bar": [
		(1, 1, 1, 1),
		(0, 0, 0, 0)
	],
	"ll": [
		(1, 1, 1, 0),
		(0, 0, 1, 0)
	],
	"lr": [
		(1, 1, 1, 0),
		(1, 0, 0, 0)
	],
	"s": [
		(0, 1, 1, 0),
		(1, 1, 0, 0)
	],
	"square": [
		(0, 1, 1, 0),
		(0, 1, 1, 0)
	],
	"t": [
		(1, 1, 1, 0),
		(0, 1, 0, 0)
	],
	"z": [
		(1, 1, 0, 0),
		(0, 1, 1, 0)
	]
}

current_map = [[0 for _ in range(0,10)] for _ in range(18)]

piece_colours = {'square': (255, 0, 0), 'bar': (255, 102, 0), 'll': (204, 0, 255), 's': (102, 204, 255), 'lr': (0, 0, 255), 't': (255, 255, 0), 'z': (0, 255, 0)}

def keypress(sequence):
    p = Popen(['xte'], stdin=PIPE)
    p.communicate(input=sequence)

def complete_move():
	keypress("key space\n")

def move_l():
	keypress("key Left\n")

def move_r():
	keypress("key Right\n")

def rotate_l():
	keypress("key z\n")

def rotate_r():
	keypress("key x\n")

def matrix_rotate_cw(matrix):
	return zip(*matrix[::-1])

def matrix_rotate_anti_cw(matrix):
	return zip(*matrix)[::-1]

def grab_screen():
	w = gtk.gdk.get_default_root_window()
	pb = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, False, 8, 625, 620)
	pb = pb.get_from_drawable(w, w.get_colormap(), 2560, 200, 0, 0, 625, 620)
	return Image.fromstring("RGB", (pb.get_width(), pb.get_height()), pb.get_pixels(), 'raw', 'RGB', pb.get_rowstride(), 1)

detected_start = base_color_start

def round():
	# Whats on the screen?
	im = grab_screen()
	for key in piece_colours.keys():
		detected_start = im.getpixel(key_coords_start)
		if piece_colours[key] == detected_start:
			print "%s piece detected" % key
		if piece_colours[key] == im.getpixel(key_coords):
			print "%s piece detected next" % key

while True:
	round()
	time.sleep(0.3)
	im = grab_screen()
	start = detected_start
	while detected_start == start or im.getpixel(key_coords_start) == base_color_start:
		time.sleep(0.3)
		im = grab_screen()
		start = im.getpixel(key_coords_start)